var date = new Date(),
   day = date.getDate(),
   month = date.getMonth(),
   monthsName = ['Styczeń',
                 'Luty',
                 'Marzec',
                 'Kwiecień',
                 'Maj',
                 'Czerwiec',
                 'Lipiec',
                 'Sierpień',
                 'Wrzesień',
                 'Październik',
                 'Listopad',
                 'Grudzień'];
var dateContainer = document.getElementById('dateContainer');
dateContainer.innerHTML = day + ' ' + monthsName[month];
console.log(day);
