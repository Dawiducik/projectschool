'use strict';
var gulp = require('gulp'),
   sass = require('gulp-sass'),
   rename = require('gulp-rename'),
   cssmin = require('gulp-cssmin'),
   cssbeautify = require('gulp-cssbeautify');
   
gulp.task('sass', function () {
   return gulp.src('sources/styles/*.scss')
         .pipe(sass().on('error', sass.logError))
         .pipe(cssbeautify({
            indent: '  ',
            openbrace: 'separate-line',
            autosemicolon: true
          }))
         .pipe(gulp.dest('public/css'))
         .pipe(cssmin())
         .pipe(rename({suffix: '.min'}))
         .pipe(gulp.dest('public/css'));
});

gulp.task('sass:watch', function () {
   gulp.watch('sources/styles/*.scss',['sass']);
   
});